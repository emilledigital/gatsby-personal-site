/**
 * Configure your Gatsby site with this file.
 *
 * See: https://www.gatsbyjs.org/docs/gatsby-config/
 */

const dotenv = require('dotenv');
dotenv.config();

module.exports = {
  /* Your site config here */
  siteMetadata: {
    title: `Emille G.`,
    description:'Software developer specializing in Python, Django and Node.js',
    author: `@emilledigital`,
  },

  plugins: [
    {
      resolve: `gatsby-plugin-typography`,
      options: {
        pathToConfigModule: `src/utils/typography`,
      },
    },

    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        // replace "UA-XXXXXXXXX-X" with your own Tracking ID
        trackingId: "UA-XXXXXXXXX-X",
      },
    },

    'gatsby-plugin-react-helmet',

    `gatsby-plugin-transition-link`,

    {
      resolve: `gatsby-source-wordpress`,
      options: {
        baseUrl: `egxdigital.wordpress.com`,
        protocol: `https`,
        hostingWPCOM: true,
        useACF: false,
        acfOptionPageIds: [],

        auth: {
          htaccess_user: process.env.HTACCESS_USER,
          htaccess_pass: process.env.HTACCESS_PASS,
          htaccess_sendImmediately: false,
          wpcom_app_clientSecret: process.env.WP_CLIENT_SECRET,
          wpcom_app_clientId: process.env.WP_CLIENT_ID,
          wpcom_user: process.env.WP_USER,
          wpcom_pass: process.env.WP_PASS,
        },
        
        verboseOutput: true,
        perPage: 100,
        searchAndReplaceContentUrls: {
          sourceUrl: "https://egxdigital.wordpress.com",
          replacementUrl: "https://egxdigital.com",
        },
        concurrentRequests: 10,
        includedRoutes: [
          "**/categories",
          "**/posts",
          //"**/pages",
          "**/media",
          "**/tags",
          //"**/taxonomies",
          //"**/users",
        ],
        excludedRoutes: ["**/posts/1456"],
        keepMediaSizes: true,
        normalizer: function({ entities }) {
          return entities
        },
      },
    },

  ],
}
