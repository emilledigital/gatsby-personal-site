const path = require(`path`)
const yaml = require("js-yaml")
const fs = require("fs")

exports.createPages = ({ graphql, actions }) => {
  const { createPage } = actions
  return graphql(`
    {
      allWordpressPost(sort: { fields: [date] }) {
        edges {
          node {
            title
            excerpt
            content
            slug
          }
        }
      }
    }
  `).then(result => {
    const posts = result.data.allWordpressPost.edges

    posts.forEach(({ node }, index) => {
      createPage({
        path: node.slug,
        component: path.resolve(`./src/templates/blog-post.js`),
        context: {
          // This is the $slug variable
          // passed to blog-post.js
          slug: node.slug,
          /*
          prev: index === 0 ? null : posts[index-1].node,
          next: index === (posts.length-1) ? null : posts[index+1].node,
          */
        },
      })
    })
  })
}
