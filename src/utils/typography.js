import Typography from "typography"
import CodePlugin from 'typography-plugin-code'
import moragaTheme from 'typography-theme-moraga'

moragaTheme.plugins = [
  new CodePlugin()
]

const typography = new Typography(
  moragaTheme
)

export const { scale, rhythm, options } = typography

export default typography
