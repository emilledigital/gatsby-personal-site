import React from "react"
import PropTypes from "prop-types"
import Headerlabel from "./headerlabel"
import Innerslot from "./innerslot.js"
import slotPostStyles from "./slot.module.css"

function slotPost({ title, company, duration }) {
  return (
    <div className={slotPostStyles.slotPost}>
      <Innerslot company={company} duration={duration}/>
      <div><h4 style={{fontSize:`20px`}}>{title}</h4></div>
    </div>
  )
}

slotPost.defaultProps = {
  title: null,
  company: null,
  duration: null,
}

slotPost.propTypes = {
  title: PropTypes.string,
  company: PropTypes.string,
  duration: PropTypes.string,
}

export default slotPost
