import React from "react"
import cardbodyStyles from "./cardbody.module.css"

export default ({children}) => (
  <div className={cardbodyStyles.cardbody}>
    {children}
  </div>
)
