import React from "react"
import cardStyles from "./card.module.css"

export default ({children}) => (
  <div className={cardStyles.card}>
    {children}
  </div>
)
