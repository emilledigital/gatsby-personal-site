import React from "react"
import PropTypes from "prop-types"
import latestStyles from "./latest.module.css"

export default ({children}) => (
    <ul className={latestStyles.latest}>
      {children}
    </ul>
)
