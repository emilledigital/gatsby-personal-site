import React from "react"
import PropTypes from "prop-types"
import headerlabelStyles from "./headerlabel.module.css"

function headerlabel({ text }) {
  return (
    <h4 className={headerlabelStyles.headerlabel}>
      {text}
    </h4>
  )
}

headerlabel.defaultProps = {
  text: `<placeholder>`,
}

headerlabel.propTypes = {
  text: PropTypes.string,
}

export default headerlabel
