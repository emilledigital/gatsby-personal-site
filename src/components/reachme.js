import React from "react"
import PropTypes from "prop-types"
import {Link} from "gatsby"
import ResumeSectionHeader from "./resumeSectionHeader"
import reachmeStyles from "./reachme.module.css"

function reachme({ phone, email, twitter, linkedin }) {
  return (
    <ul className={reachmeStyles.reachme}>
      <li>
        <a href={"tel:"+phone}><h6>{phone}</h6></a>
      </li>
      <li>
        <a href={"mailto:"+email}><h6>{email}</h6></a>
      </li>
      <li>
        <Link to="/"><img style={{margin:`0.2em 0 0 0`, height:`24px`, width:`24px`}}src="https://i.ibb.co/VQgVmQP/globe-grid.png"/></Link>
      </li>
    </ul>
  )
}

reachme.defaultProps = {
  phone: `(310) 651-8744`,
  email: `emilledigital@gmail.com`,
  twitter: `@emilledigital`,
  linkedin: `linkedin.com/in/egx`,
}

reachme.propTypes = {
  phone: PropTypes.string,
  email: PropTypes.string,
  twitter: PropTypes.string,
  linkedin: PropTypes.string,
}

export default reachme
