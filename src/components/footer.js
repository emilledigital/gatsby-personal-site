import React from "react"
import footerStyles from "./footer.module.css"

export default ({children}) => (
  <div className={footerStyles.footer}>
    {children}
  </div>
)
