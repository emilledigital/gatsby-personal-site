import React from "react"
import resumeStyles from "./resume.module.css"

export default ({children}) => (
  <div className={resumeStyles.resume}>
    {children}
  </div>
)
