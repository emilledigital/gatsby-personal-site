import React from "react"
import blogRunnerStyles from "./blogRunner.module.css"

export default ({children}) => (
  <div className={blogRunnerStyles.blogRunner}>
    {children}
  </div>
)
