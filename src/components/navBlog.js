import React from "react"
import navBlogStyles from "./navBlog.module.css"

export default ({ children }) => (
    <div className={navBlogStyles.navBlog}>{children}</div>
)
