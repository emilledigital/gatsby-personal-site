import React from "react"
import {Link} from "gatsby"
import PropTypes from "prop-types"
import menuStyles from "./menu.module.css"

function menu({ home, one, two, three }) {
  return (
    <ul className={menuStyles.menu}>
        <li>
          <img src="https://i.ibb.co/pP4HxL5/emille-g-200.png" alt="Python developer Emille G."/>
        </li>
        <li>
          <Link to="/"><h4>{home}</h4></Link>
        </li>
        <li>
          <Link to="/resources/"><h4>{one}</h4></Link>
        </li>
    </ul>
  )
}

menu.defaultProps = {
  one: ``,
  home: ``,
  two: ``,
  three: ``,
}

menu.propTypes = {
  home: PropTypes.string,
  one: PropTypes.string,
  two: PropTypes.string,
  three: PropTypes.string,
}

export default menu
