import React from "react"
import PropTypes from "prop-types"
import taglineStyles from "./tagline.module.css"

function tagline({ text }) {
  return (
    <h5 className={taglineStyles.text}>
      {text}
    </h5>
  )
}

tagline.defaultProps = {
  text: `<Placeholder>`,
}

tagline.propTypes = {
  text: PropTypes.string,
}

export default tagline
