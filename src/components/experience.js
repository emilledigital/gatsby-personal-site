import React from "react"
import experienceStyles from "./experience.module.css"

export default ({children}) => (
  <div className={experienceStyles.experience}>
    {children}
  </div>
)
