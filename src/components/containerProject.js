import React from "react"
import containerProjectStyles from "./containerProject.module.css"

export default ({children}) => (
  <div className={containerProjectStyles.containerProject}>
    {children}
  </div>
)
