import React from "react"
import detailStyles from "./detail.module.css"

export default ({children}) => (
  <div className={detailStyles.detail}>
    {children}
  </div>
)
