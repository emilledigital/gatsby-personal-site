import React from "react"
import summaryToolsStyles from "./summaryTools.module.css"

function summaryTools({ text, data }) {
  return (
    <div className={summaryToolsStyles.summaryTools}>

    <h4 style={{textAlign:`left`, marginRight:`0.414em`}}>{text}</h4>

      <ul>
        {data.map((value, i) => (
            <li>
              <h5>{value}</h5>
            </li>
        ))}
      </ul>
    </div>
  )
}

export default summaryTools
