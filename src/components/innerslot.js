import React from "react"
import PropTypes from "prop-types"
import innerslotStyles from "./innerslot.module.css"

function innerslot({ title, company, duration }) {
  return (
    <div className={innerslotStyles.innerslot}>
        <h6>{company}</h6>
        <h6>{duration}</h6>
    </div>
  )
}

innerslot.defaultProps = {
  company: `<Placeholder>`,
  duration: `<Placeholder>`,
}

innerslot.propTypes = {
  company: PropTypes.string,
  duration: PropTypes.string,
}

export default innerslot
