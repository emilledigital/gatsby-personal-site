import React from "react"
import {Link} from "gatsby"
import SummaryTools from "../components/summaryTools"
import summaryStyles from "./summary.module.css"

function summary({ text }) {
  return (
    <div className={summaryStyles.summary}>
        <h4>
          {text} Be sure to check out the <Link to="/projects">projects I'm currently working on</Link>.
        </h4>

        <SummaryTools text="Current technologies" data={[
          "Python",
          "Django",
          "PostgreSQL",
          "JavaScript",
          "Node.js",
        ]}/>
    </div>
  )
}

export default summary
