import React from "react"
import containerBlogStyles from "./containerBlog.module.css"

export default ({children}) => (
  <div className={containerBlogStyles.containerBlog}>
    {children}
  </div>
)
