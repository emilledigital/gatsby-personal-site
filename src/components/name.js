import React from "react"
import PropTypes from "prop-types"

function name( { text } ) {
  return (
    <h3 style={{textAlign:`center`}}>
      {text}
    </h3>
  )
}

name.defaultProps = {
  text: `<Placeholder>`,
}

name.propTypes = {
  text: PropTypes.string,
}

export default name
