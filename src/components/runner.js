import React from "react"
import runnerStyles from "./runner.module.css"

export default ({children}) => (
  <div className={runnerStyles.runner}>
    {children}
  </div>
)
