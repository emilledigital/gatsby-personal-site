import React from "react"
import PropTypes from "prop-types"
import featuresStyles from "./features.module.css"

function features({ data }) {
  return (
    <ul className={featuresStyles.features}>
      {data.map((value, i) => (
          <li>
            <h5>{value}</h5>
          </li>
      ))}
    </ul>
  )
}

export default features
