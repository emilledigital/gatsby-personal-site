import React from "react"
import PropTypes from "prop-types"

function resumeSectionHeader({ text }) {
  return (
    <h5 style={{margin:`0 0 0.5em 0`}}>
      {text}
    </h5>
  )
}

resumeSectionHeader.defaultProps = {
  resumeSectionHeader: `<Placeholder>`,
}

resumeSectionHeader.propTypes = {
  resumeSectionHeader: PropTypes.string,
}

export default resumeSectionHeader
