import React from "react"
import containerHomeStyles from "./containerHome.module.css"

export default ({children}) => (
  <div className={containerHomeStyles.containerHome}>
    {children}
  </div>
)
