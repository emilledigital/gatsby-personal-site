import React from "react"
import PropTypes from "prop-types"
import contactStyles from "./contact.module.css"
import ContactContainer from "./contactContainer"

function contact({ phone, email, twitter, linkedin }) {
  return (
    <ContactContainer>
      <ul className={contactStyles.contact}>
        <li>
          <img src="https://i.ibb.co/bNWC4JR/005-phone-call.png" alt="phone" />
          <a href={"tel:"+phone}><h4>{phone}</h4></a>
        </li>
        <li>
          <img src="https://i.ibb.co/QPXHWWn/002-logo.png" alt="email" />
          <a href={"mailto:"+email}><h4>{email}</h4></a>
        </li>
      </ul>
      <ul className={contactStyles.contact}>
        <li>
          <img src="https://i.ibb.co/zhhd406/003-twitter.png" alt="twitter" />
          <a target="_blank" href="https://twitter.com/emilledigital"><h4>{twitter}</h4></a>
        </li>
        <li>
          <img src="https://i.ibb.co/Gpk9hT1/004-linkedin.png" alt="linkedin" />
          <a target="_blank" href="https://www.linkedin.com/in/egx/"><h4>{linkedin}</h4></a>
        </li>
      </ul>
    </ContactContainer>
  )
}

contact.defaultProps = {
  phone: `(310) 651-8744`,
  email: `emilledigital@gmail.com`,
  twitter: `@emilledigital`,
  linkedin: `linkedin.com/in/egx`,
}

contact.propTypes = {
  phone: PropTypes.string,
  email: PropTypes.string,
  twitter: PropTypes.string,
  linkedin: PropTypes.string,
}

export default contact
