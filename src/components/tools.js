import React from "react"
import toolsStyles from "./tools.module.css"

function tools({ data }) {
  return (
    <ul className={toolsStyles.tools}>
      {data.map((value, i) => (
          <li>
            <h5>{value}</h5>
          </li>
      ))}
    </ul>
  )
}

export default tools
