import React from "react"
import welcomeStyles from "./welcome.module.css"

export default props =>
<div className={welcomeStyles.welcome}>
  <h1>{props.greeting}</h1>
</div>
