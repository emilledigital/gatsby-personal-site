import React from "react"
import PropTypes from "prop-types"
import headlineStyles from "./headline.module.css"

function headline({ text }) {
  return (
    <h3 className={headlineStyles.headline}>
      {text}
    </h3>
  )
}

headline.defaultProps = {
  text: `<Placeholder>`,
}

headline.propTypes = {
  text: PropTypes.string,
}

export default headline
