import React from "react"
import PropTypes from "prop-types"
import notablesStyles from "./notables.module.css"

function notables({ data }) {
  return (
    <ul className={notablesStyles.notables}>
      {data.map((value, i) => (
        <li>
          <h4>{value}</h4>
        </li>
      ))}
    </ul>
  )
}

export default notables
