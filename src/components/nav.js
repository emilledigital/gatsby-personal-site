import React from "react"
import navStyles from "./nav.module.css"
import {TransitionPortal} from "gatsby-plugin-transition-link"

export default ({ children }) => (
  <div className={navStyles.nav}>{children}</div>
)
