import React from "react"
import PropTypes from "prop-types"
import Headerlabel from "./headerlabel"
import Innerslot from "./innerslot.js"
import slotStyles from "./slot.module.css"

function slot({ title, company, duration }) {
  return (
    <div className={slotStyles.slot}>
      <div><Headerlabel text={title}/></div>
      <Innerslot company={company} duration={duration}/>
    </div>
  )
}

slot.defaultProps = {
  title: null,
  company: null,
  duration: null,
}

slot.propTypes = {
  title: PropTypes.string,
  company: PropTypes.string,
  duration: PropTypes.string,
}

export default slot
