import React from "react"
import PropTypes from "prop-types"
import projectlinksStyles from "./projectlinks.module.css"

function projectlinks({ data }) {
  return (
    <ul className={projectlinksStyles.projectlinks}>
      {data.map(({key:key,value:value}, i) => (
        <li>
          <a target="_blank" href={value}><h5>{key}</h5></a>
        </li>
      ))}
    </ul>
  )
}

export default projectlinks
