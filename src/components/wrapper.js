import React from "react"
import wrapperStyles from "./wrapper.module.css"

export default ({children}) => (
  <div className={wrapperStyles.wrapper}>
    {children}
  </div>
)
