import React from "react"
import {Link} from "gatsby"
import blogMenuStyles from "./blogMenu.module.css"

function blogMenu({ data }) {
  return (
    <ul className={blogMenuStyles.blogMenu}>
      {data.map(({key:key,value:value}, i) => (
        <li>
          <Link to={value}><h5>{key}</h5></Link>
        </li>
      ))}
    </ul>
  )
}

export default blogMenu
