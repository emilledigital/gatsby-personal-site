import React from "react"
import contactContainerStyles from "./contactContainer.module.css"

export default ({children}) => (
  <div className={contactContainerStyles.contactContainer}>
    {children}
  </div>
)
