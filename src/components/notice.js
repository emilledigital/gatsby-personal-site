import React from "react"
import noticeStyles from "./notice.module.css"

export default ({children}) => (
  <div className={noticeStyles.notice}>
    {children}
  </div>
)
