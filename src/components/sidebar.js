import React from "react"
import sidebarStyles from "./sidebar.module.css"

export default ({children}) => (
  <div className={sidebarStyles.sidebar}>
    {children}
  </div>
)
