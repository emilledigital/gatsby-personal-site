function htmlEntities(str) {
    return String(str).replace(/&nbsp;/g, " ").replace(/&#8211;/g, "-");
}

export default htmlEntities
