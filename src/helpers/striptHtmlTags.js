function strip_html_tags(str)
{
  if ((str===null) || (str===''))
       return false;
  else
   str = str.toString();
   str = str.replace(/<[^>]*>|#8211|&|;/g, '');
  return str.replace(/nbsp/g, ' ');
}

export default strip_html_tags
