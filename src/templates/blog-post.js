import React from "react"
import SEO from "../components/seo"
import striphtmlEntity from "../helpers/htmlEntity"
import strip_html_tags from "../helpers/striptHtmlTags"
import currentYear from "../helpers/currentYear"
import ContainerBlog from "../components/containerBlog"
import BlogRunner from "../components/blogRunner"
import NavBlog from "../components/navBlog"
import BlogMenu from "../components/blogMenu"
import Footer from "../components/footer"
import { graphql } from "gatsby"
import "../styles/global.css"

export default ({ data }) => {
  const post = data.allWordpressPost.edges[0].node
  return (
    <ContainerBlog>
      <SEO title={striphtmlEntity(post.title)} description={strip_html_tags(striphtmlEntity(post.excerpt))} keywords={post.categories[0].name}/>
      <BlogRunner>
        <NavBlog>
          <BlogMenu data={[
            {key:"main", value:"/"},
            {key:"read more", value:"/resources"},
          ]}/>
        </NavBlog>
        <h1>{striphtmlEntity(post.title)}</h1>
        <div dangerouslySetInnerHTML={{ __html: post.content }} />
        <NavBlog>
          <BlogMenu data={[
            {key:"read more", value:"/resources"},
          ]}/>
        </NavBlog>
        <Footer>
          <h5>&copy; 2019-{currentYear} Emille G.</h5>
        </Footer>
      </BlogRunner>
    </ContainerBlog>
  )
}
export const query = graphql`
  query($slug: String!) {
    allWordpressPost(filter: { slug: { eq: $slug } }) {
      edges {
        node {
          title
          content
          excerpt
          categories {
            name
          }
        }
      }
    }
  }
`
