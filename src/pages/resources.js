import React from "react"
import SEO from "../components/seo"
import {Link} from "gatsby"
import striphtmlEntity from "../helpers/htmlEntity"
import currentYear from "../helpers/currentYear"
import "../styles/global.css"
import Wrapper from "../components/wrapper"
import ContainerHome from "../components/containerHome"
import Nav from "../components/nav"
import Menu from "../components/menu"
import Welcome from "../components/welcome"
import Detail from "../components/detail"
import Headline from "../components/headline"
import Summary from "../components/summary"
import Contact from "../components/contact"
import Notice from "../components/notice"
import Latest from "../components/latest"
import SlotPost from "../components/slotPost"
import SpacerVertical from "../components/spacerVertical"
import Footer from "../components/footer"

export default ({data}) => (
  <Wrapper>
  <ContainerHome>
      <SEO title="Python tutorials and more" description="Learn Python, Django, PostgreSQL and more." keywords="Python tutorials"/>
      <Nav>
        <Menu home="emille g." one="resources"/>
      </Nav>

      <Welcome greeting="Articles and tutorials" />

      <Notice>
        <Headline text="Blog"/>
        <Latest>
          {data.articles.edges.map(({ node }) => (
            <li>
              <Link to={node.slug}>
                <SlotPost title={striphtmlEntity(node.title)} company={node.date} duration={null}/>
              </Link>
            </li>
          ))}
        </Latest>
      </Notice>

      <Notice>
          <Headline text="Python tutorials"/>
          <Latest>
            {data.pythonTutorials.edges.map(({ node }) => (
              <li>
                <Link to={node.slug}>
                  <SlotPost title={striphtmlEntity(node.title)} company={node.date} duration={null}/>
                </Link>
              </li>
            ))}
          </Latest>
      </Notice>

      <Notice>
        <Headline text="Guides"/>
        <Latest>
          {data.guides.edges.map(({ node }) => (
            <li>
              <Link to={node.slug}>
                <SlotPost title={striphtmlEntity(node.title)} company={node.date} duration={null}/>
              </Link>
            </li>
          ))}
        </Latest>
      </Notice>
    </ContainerHome>
    <Footer>
      <h5>&copy; 2019-{currentYear} Emille G.</h5>
    </Footer>
  </Wrapper>
)

export const pageQuery = graphql`
query {

  metaData: site {
    siteMetadata {
      title
      description
    }
  }

  articles: allWordpressPost(
    sort: { fields: [date], order: DESC }
    filter: {categories:{elemMatch:{name:{eq:"Article"}}}}
  )
  {
    edges {
      node {
        title
        date (formatString: "dddd, D")
        excerpt
        slug
      }
    }
  }

  guides: allWordpressPost(
    sort: { fields: [date], order: DESC }
    filter: {categories:{elemMatch:{name:{eq:"Guide"}}}}
  )
  {
    edges {
      node {
        title
        date (formatString: "dddd, D")
        excerpt
        slug
      }
    }
  }

  pythonTutorials: allWordpressPost(
    sort: { fields: [date]}
    filter: {categories:{elemMatch:{name:{eq:"Python tutorial"}}}}
  )
  {
    edges {
      node {
        title
        date (formatString: "dddd, D")
        excerpt
        slug
      }
    }
  }
}
`
