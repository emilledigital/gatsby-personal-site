import React from "react"
import SEO from "../components/seo"
import { Link, graphql } from "gatsby"
import striphtmlEntity from "../helpers/htmlEntity"
import currentYear from "../helpers/currentYear"
import Wrapper from "../components/wrapper"
import ContainerHome from "../components/containerHome"
import Nav from "../components/nav"
import Menu from "../components/menu"
import Welcome from "../components/welcome"
import Detail from "../components/detail"
import Headline from "../components/headline"
import Summary from "../components/summary"
import Contact from "../components/contact"
import SummaryTools from "../components/summaryTools"
import Notice from "../components/notice"
import Latest from "../components/latest"
import Slot from "../components/slot"
import SpacerVertical from "../components/spacerVertical"
import Footer from "../components/footer"

export default ({data}) => (
  <Wrapper>
    <ContainerHome>
    <SEO title="Python developer" description="Hire a developer full time remote for Python, Django, Node.js and PostgreSQL development" keywords="Python developer"/>
      <Nav>
        <Menu home="emille g." one="resources"/>
      </Nav>

      <Welcome greeting="Hi, I'm Emille" />

      <Detail>
        <Headline text="Python developer based in South America delivering remotely worldwide. Hire me!" />
        <Summary text=""/>
      </Detail>

      <Contact/>

      <Notice>
          <Headline text="Latest projects" />
          <Latest>
            <li>
              <Slot title="Gatsby personal site" company="Gatsby" duration=""/>
            </li>
            <li>
              <Slot title="Python knowledge management" company="Django" duration=""/>
            </li>
            <li>
              <Slot title="Stack Overflow Jobs" company="BeautifulSoup" duration=""/>
            </li>
          </Latest>
      </Notice>

      <Notice>
          <Headline text="Latest posts" />
          <Latest>
            {data.posts.edges.map(({ node }) => (
              <li>
                <Link to={node.slug}>
                  <Slot title={striphtmlEntity(node.title)} company={node.categories[0].name} duration={null}/>
                </Link>
              </li>
            ))}
          </Latest>
      </Notice>

    </ContainerHome>
    <Footer>
      <h5>&copy; 2019-{currentYear} Emille.G</h5>
    </Footer>
  </Wrapper>
)

export const pageQuery = graphql`
  query
  {
    metaData: site {
      siteMetadata {
        title
        description
      }
    }

    posts: allWordpressPost (
      sort: { fields: [date], order: DESC }
      limit: 3
    )
    {
      edges {
        node {
          title
          slug
          categories {
            name
          }
        }
      }
    }
  }
`
