import React from "react"
import SEO from "../components/seo"
import currentYear from "../helpers/currentYear"
import Wrapper from "../components/wrapper"
import ContainerProject from "../components/containerProject"
import Sidebar from "../components/sidebar"
import Resume from "../components/resume"
import ResumeSectionHeader from "../components/resumeSectionHeader"
import Name from "../components/name"
import Headerlabel from "../components/headerlabel"
import Tagline from "../components/tagline"
import Tools from "../components/tools"
import Experience from "../components/experience"
import Reachme from "../components/reachme"
import Slot from "../components/slot"
import Runner from "../components/runner"
import Card from "../components/card"
import Cardbody from "../components/cardbody"
import Features from "../components/features"
import Notables from "../components/notables"
import Projectlinks from "../components/projectlinks"
import SpacerVertical from "../components/spacerVertical"
import CardSpacer from "../components/cardSpacer"
import Footer from "../components/footer"



export default () => (
  <ContainerProject>
    <SEO title="Python developer portfolio" description="Hire a developer full time remote for Python, Django, Node.js and PostgreSQL development" keywords="Python developer"/>
    <Sidebar>
      <Resume>
        <img src="https://i.ibb.co/Hd1PVFf/eHi, I'm Emilleg-rotated.jpg" alt="" />
        <Name text="Emille G"/>
        <Headerlabel text="Python Developer"/>
        <Tagline text="Achieving excellence with thoughtful design decisions"/>
        <ResumeSectionHeader text="expertise"/>
        <Tools data={["Python", "JavaScript", "Django", "Node.js", "Web development", "Web scraping", "Scripting"]}/>
        <ResumeSectionHeader text="experience"/>
        <Experience>
          <Slot title="Python developer" company="Enrapt Digital" duration="Apr 2018-present"/>
          <Slot title="Content Editor" company="Consult PR Inc." duration="Apr 2017-Apr 2018"/>
          <Slot title="Economic and Financial Analyst" company="Ministry of Finance" duration="Feb 2014-Aug 2016"/>
        </Experience>
        <Reachme />
      </Resume>
    </Sidebar>

    <Runner>
    <Card>
      <img src="https://i.ibb.co/16Y0X9p/Screenshot-20191203-183918.png" alt="A personal site made with Gatsby.js"/>
      <Cardbody>
        <Name text="Gatsby personal site"/>
        <SpacerVertical></SpacerVertical>
        <Features data={["Gatsby.js"]}/>
        <Headerlabel text="Finally, a personal web space for me to share some of the value I create."/>
        <SpacerVertical></SpacerVertical>
        <Headerlabel text="Achievements"/>
        <Notables data={[
          "- Fully custom",
          "- Fully mobile responsive",
          "- Connected to Wordpress",
        ]}/>
        <Headerlabel text="Links"/>
        <Projectlinks data={[
          {key:"repository", value:"https://www.example.com"},
        ]}/>
      </Cardbody>
    </Card>
      <Card>
        <img src="https://i.ibb.co/NsYbQsS/library-869061-1920.jpg" alt="Personal knowledge management Django app"/>
        <Cardbody>
          <Name text="Personal knowledge management"/>
          <SpacerVertical></SpacerVertical>
          <Features data={["Python", "Django", "Django REST Framework", "React", "PostgreSQL"]}/>
          <Headerlabel text="Assign notes to study sessions, projects, books and courses."/>
          <SpacerVertical></SpacerVertical>
          <Headerlabel text="Achievements"/>
          <Notables data={[
            "- React front end consuming a Django REST API"
          ]}/>
          <Headerlabel text="Links"/>
          <Projectlinks data={[
            {key:"pending", value:""},
          ]}/>
        </Cardbody>
      </Card>
      <Card>
        <img src="https://i.ibb.co/kB91Bpc/black-samsung-tablet-computer-106344.jpg" alt="Stack Overflow Jobs data visualization project"/>
        <Cardbody>
          <Name text="Stack Overflow Jobs"/>
          <SpacerVertical></SpacerVertical>
          <Features data={["Python", "BeautifulSoup", "PostgreSQL"]}/>
          <Headerlabel text="A data visualization project powered by an automated web scraper"/>
          <SpacerVertical></SpacerVertical>
          <Headerlabel text="Achievements"/>
          <Notables data={[
            "- Fully re-usable codebase",
            "- Selenium webdriver functions for getting past login pages in Python"
          ]}/>
          <Headerlabel text="Links"/>
          <Projectlinks data={[
            {key:"pending", value:""},
          ]}/>
        </Cardbody>
      </Card>


      <Footer>
        <h5>&copy; 2019-{currentYear} Emille G.</h5>
      </Footer>
    </Runner>
  </ContainerProject>
)
